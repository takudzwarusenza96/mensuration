import math
def circle(radius):
    option=0
    while option not in (1,2):
        option= int(input("1.  Area of a Circle \n2. Circumference of a Circle: "))
                         
    if (option==1):
        area= (radius **2) * math.pi
        print(f'The area is: {area}')
    else:
        perimeter = math.pi * 2 * radius
        print(f'The Perimeter is :{perimeter}')


def triangle(base, height):
    option = 0
    
    while option not in (1,2):
        print("Assuming its a right-angled triangle \n")
        option=int(input("1.Area of a Triangle \n2. Perimeter of a Triangle: "))
        
    if option == 1:
        area = (base*height)/2
        print(f'The area is: {area}')
    else:
        hypo = math.sqrt*(base**2 + height**2)
        perimeter = hypo + base + height
        print(f'The Perimeter is :{perimeter}')

    
def square(side):
    option = 0
    while option not in (1,2):
        option= int(input("1. Area of a square \n2. Perimeter of a square: "))
    
    if option== 1:
        area = side**2
        print(f'The area is: {area}')
    else:
        perimeter = 4*side
        print(f'The Perimeter is :{perimeter}')
    
    
def rectangle(length, width):
    option = 0
    while option not in (1,2):
        option = int(input("1. Area of a rectangle \n2. Perimeter of rectangle: "))
     
    if option==1 :
        area = length*width
        print(f'The area is: {area}')
    else:
        perimeter = 2*length + 2*width
        print(f'The Perimeter is :{perimeter}')

                           
def sphere(radius):
    option = 0
    while option not in (1,2):
        option = int(input("1. Volume of sphere \n2. Total surface area of sphere: "))
    if option == 1:
        volume = 4/3*math.pi * (radius**3)
        print(f'The Volume is :{volume}' )
    
    else:
        total_surface_area = 4*math.pi* (radius**2)
        print(f"The total surface Area is: {total_surface_area}" )
        
                           
def cuboid(length , width , height):
    option = 0
    while option not in (1,2):
        option =int(input("1. Volume of cuboid \n2. Total surface area of cuboid: "))
        
    if option == 1 :
        volume = length*width*height
        print(f'The Volume is :{volume}' )
    else :
        total_surface_area = 2*((length*width)+ (length*height) +(height*width))
        print(f"The total surface Area is: {total_surface_area}" )
    

def cube(side):
    option = 0
    while option not in (1,2):
        option= int(input("1. Volume of cube \n2. Total surface area of cube: "))
    
    if option == 1:        
        volume = side**3
        print(f'The Volume is :{volume}' )    
    else:
        total_surface_area = 6 * side
        print(f"The total surface Area is: {total_surface_area}" )
        
    
def pyramid(base_area , height , lenght_of_any_slant_side, number_of_sides):
    option = 0
    while option not in (1,2):
        option =int(input(" 1. Volume of pyramid \n2. Total surface area of pyramid: "))
      
    if option == 1:
        volume = 1/3*(base_area *height)
        print(f'The Volume is :{volume}' )
    else:
        side = base_area/number_of_sides
        perimeter = number_of_sides*side
        apothem = lenght_of_any_slant_side/(2*math.tan(math.pi/number_of_sides))
        total_surface_area = 1/2*(perimeter * apothem) + base_area
        print(f"The total surface Area is: {total_surface_area}" )

decision=True

while decision==True:
    function_required = int(input("""What function do you want :
                                    \n 1. Circle Mensuration
                                    \n 2. Triangle mensuration
                                    \n 3. Rectangle mensuration
                                    \n 4. Square Mensuration
                                    \n 5. Cube Mensuration
                                    \n 6. Cuboid Mensuartion
                                    \n 7. Sphere Mensuration
                                    \n
                                    """))
        
    if function_required ==1 :
        radius = int(input("Enter radius :"))
        circle(radius)
    elif function_required==2 :
        base =int(input("Enter base :"))
        height = int(input("Enter height :"))
        triangle(base,height)
    elif function_required ==3 :
        length =int(input("Enter length :"))
        width = int(input("Enter width :"))
        rectangle(length, width)
    elif function_required ==4 :
        side = int(input("Enter side :"))
        square(side)
    elif function_required ==5 :
        side = int(input("Enter side :"))
        cube(side)
    elif function_required ==6 :
        length =int(input("Enter length :"))
        width = int(input("Enter width :"))
        height = int(input("Enter height"))
        cuboid(length, width , height)
    elif function_required ==7 :
        radius = int(input("Enter radius :"))
        sphere(radius)    
    
    prompt=input("Press \nY  if you want to continue   \nN  if you want to quit : ")
    if prompt in ('y','Y'):
        decision=True
    elif prompt in ('n','N'):
        decision=False
    else:
        print('Invalid entry')
        decision=False

 
